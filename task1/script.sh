#!/bin/bash
set -e

source variables

function success_msg {

echo "Successful install" | mail -s "Vizor_Games" test@gmail.com
}

function error_msg {

echo "Error during installation." | mail -s "Vizor_Games" test@gmail.com
}

function install_and_config_ssmtp {

apt-get update
apt-get autoremove -y
apt-get install ssmtp mailutils -y
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/54068810/repository/files/task1%2Fssmtp%2Fssmtp/raw?ref=main" -o /etc/ssmtp/ssmtp.conf
}

function install_and_config_web_servers {

apt-get install -y apache2
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/54068810/repository/files/task1%2Fapache%2Fports/raw?ref=main" -o /etc/apache2/ports.conf
apt-get install -y libapache2-mod-php
a2dissite 000-default
service apache2 reload
apt-get install -y nginx
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/54068810/repository/files/task1%2Fnginx%2Fdefault/raw?ref=main" -o /etc/nginx/sites-available/default
systemctl restart nginx
}


function install_php {

apt install -y php7.4 php-bcmath php-curl php-imagick php-intl php-json php-mbstring php-mysql php-xml php-zip
}


function install_and_config_mysql {

sudo apt install mysql-server -y

table_exists=$(mysql -u root -e "SHOW DATABASES LIKE '$MYSQL_DATABASE';" | grep "$MYSQL_DATABASE")

if [ -z "$table_exists" ]; then
mysql -u root -e "CREATE DATABASE $MYSQL_DATABASE;"
mysql -u root -e "CREATE USER '$WORDPRESS_USER'@'localhost' IDENTIFIED BY '$WORDPRESS_PASSWORD';"
mysql -u root -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON $MYSQL_DATABASE.* TO '$WORDPRESS_USER'@'localhost';"
mysql -u root -e "FLUSH PRIVILEGES;"
fi
  }
				 
				 
function install_wordpress_and_config_apache {

apt-get install ghostscript -y
mkdir -pv /srv/www
chown www-data: /srv/www
curl https://wordpress.org/latest.tar.gz | sudo -u www-data tar zx -C /srv/www
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/54068810/repository/files/task1%2Fapache%2Fwordpress/raw?ref=main" -o /etc/apache2/sites-available/wordpress.conf
a2ensite wordpress
a2enmod rewrite
service apache2 reload
sudo -u www-data cp /srv/www/wordpress/wp-config-sample.php /srv/www/wordpress/wp-config.php
sudo -u www-data sed -i 's/database_name_here/wordpress/' /srv/www/wordpress/wp-config.php
sudo -u www-data sed -i 's/username_here/wordpress/' /srv/www/wordpress/wp-config.php
sudo -u www-data sed -i 's/password_here/password/' /srv/www/wordpress/wp-config.php
}


max_attempts=5
attempt_counter=0

while true; do
  install_and_config_ssmtp && break || ((attempt_counter++))
  if [ "$attempt_counter" -ge "$max_attempts" ]; then
    error_msg
    exit 1
  fi
  sleep 10
done

attempt_counter=0

while true; do
  install_and_config_web_servers && break || ((attempt_counter++))
  if [ "$attempt_counter" -ge "$max_attempts" ]; then
    error_msg
    exit 1
  fi
  sleep 10
done

attempt_counter=0

while true; do
  install_php && break || ((attempt_counter++))
  if [ "$attempt_counter" -ge "$max_attempts" ]; then
    error_msg
    exit 1
  fi
  sleep 10
done

attempt_counter=0

while true; do
  install_and_config_mysql && break || ((attempt_counter++))
  if [ "$attempt_counter" -ge "$max_attempts" ]; then
    error_msg
    exit 1
  fi
  sleep 10
done

attempt_counter=0

while true; do
  install_wordpress_and_config_apache && break || ((attempt_counter++))
  if [ "$attempt_counter" -ge "$max_attempts" ]; then
    error_msg
    exit 1
  fi
  sleep 10
done

success_msg

echo "The end"

#install_and_config_ssmtp
#install_and_config_web_servers
#install_php
#install_and_config_mysql
#install_wordpress_and_config_apache
#success_msg



